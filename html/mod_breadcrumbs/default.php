<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_breadcrumbs
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<ul class="breadcrumb">
	<?php
	// Get rid of duplicated entries on trail including home page when using multilanguage
	for ($i = 0; $i < $count; $i++)
	{
		if ($i === 1 && !empty($list[$i]->link) && !empty($list[$i - 1]->link) && $list[$i]->link === $list[$i - 1]->link)
		{
			unset($list[$i]);
		}
	}
	$counter = 0;
	?>
	<?php foreach ($list as $item): ?>
		<?php if ($counter === 0): ?>
            <li class="breadcrumb--item">
                <a href="<?php echo $item->link ?>" class="breadcrumb--link breadcrumb--link-home">
					<?php echo $item->name ?>
                </a>
                <span class="breadcrumb--divider"><?php echo $separator; ?></span>
            </li>
		<?php elseif ($counter === count($list) - 1): ?>
            <li class="breadcrumb--item">
                <span class="breadcrumb--link breadcrumb--link-current"><?php echo $item->name ?></span>
            </li>
		<?php else: ?>
            <li class="breadcrumb--item">
                <a href="<?php echo $item->link ?>" class="breadcrumb--link"><?php echo $item->name ?></a>
                <span class="breadcrumb--divider"><?php echo $separator ?></span>
            </li>
		<?php endif; ?>
	<?php $counter++; endforeach; ?>
</ul>