<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once JPATH_THEMES . '\promaspect\html\com_content\BaseContact.php';
/**
 * Marker_class: Class based on the selection of text, none, or icons
 * jicon-text, jicon-none, jicon-icon
 */
$contact = new BaseContact($this->contact->id);
?>

<section class="contact_us">
	<?php if (($this->params->get('address_check') > 0) &&
		($this->contact->address || $this->contact->suburb || $this->contact->state || $this->contact->country || $this->contact->postcode)) : ?>
        <div class="contact_us-container">
            <div class="icon-img-locationpoint"></div>
            <p class="contact_us--title"><?php echo JText::_('TPL_PROMASPECT_VISIT_US') ?>:</p>
            <p class="contact_us--description">
				<?php
				if ($this->contact->country && $this->params->get('show_country'))
				{
					echo "<span>{$this->contact->country}, </span>";
				}
				if ($this->contact->postcode && $this->params->get('show_postcode'))
				{
					echo "<span>{$this->contact->postcode}, </span>";
				}
				if ($this->contact->suburb && $this->params->get('show_suburb'))
				{
					echo "<span>{$this->contact->suburb},</span>";
				}
				echo '<br>';
				if ($this->contact->address && $this->params->get('show_street_address'))
				{
					echo "<span>{$this->contact->address}</span>";
				}
				?>
        </div>
	<?php endif; ?>

	<?php if ($this->contact->telephone && $this->params->get('show_telephone')) : ?>
        <div class="contact_us-container">
            <div class="icon-img-phonenumber"></div>
            <p class="contact_us--title"><?php echo JText::_('TPL_PROMASPECT_CALL_US') ?>:</p>
            <div class="contact_us--description">
				<?php foreach (explode(';', $this->contact->telephone) as $phone)
				{
					$phoneNumber = str_replace(array('(', ')', '-', ' '), '', $phone);
					echo "<a href='tel:{$phoneNumber}' class='contact_us--link'>$phone</a>";
				}
				if ($this->contact->mobile && $this->params->get('show_mobile'))
				{
					foreach (explode(';', $this->contact->mobile) as $phone)
					{
						$phoneNumber = str_replace(array('(', ')', '-', ' '), '', $phone);
						echo "<a href='tel:{$phoneNumber}' class='contact_us--link'>$phone</a>";
					}
				}
				?>
            </div>
        </div>
	<?php endif; ?>

	<?php if ($this->contact->email_to && $this->params->get('show_email')) : ?>
        <div class="contact_us-container">
            <div class="icon-img-contactmail"></div>
            <p class="contact_us--title"><?php echo JText::_('TPL_PROMASPECT_WRITE_TO_US') ?>:</p>
            <div class="contact_us--description">
				<?php foreach ($contact->baseContactFields as $field)
				{
					if ($field->name === 'mail')
					{
						foreach (explode(';', $field->value) as $mail)
						{
							echo "<a href='mailto:$mail' class='contact_us--link'>$mail</a>";
						}
						break;
					}
				} ?>
            </div>
        </div>
	<?php endif; ?>


</section>