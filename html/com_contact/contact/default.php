<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$tparams = $this->item->params;
jimport( 'joomla.application.module.helper' );
$moduleMap = JModuleHelper::getModules('position-3');
$attribs['style'] = 'none';
?>
<div class="header_title-container">
    <div class="header_title--image-container">
        <img src="/<?php echo $this->item->image ?>" alt="" class="header_title--image">
    </div>
    <div class="header_title--name-wrapper">
        <h1><?php echo $this->item->con_position == '' ? JFactory::getApplication()->getMenu()->getActive()->title : $this->item->con_position ?></h1>
    </div>
</div>
</header>

<main class="container-fluid" itemscope itemtype="https://schema.org/Person">
    <article class="container">
		<?php if ($this->contact->name && $tparams->get('show_name')) : ?>
            <h2 class="page_article--header">
				<?php echo $this->item->published == 0 ? JText::_('JUNPUBLISHED') : $this->contact->name ?>
            </h2>
		<?php endif; ?>

		<?php if ($this->contact->misc && $tparams->get('show_misc')) : ?>
            <div class="page_article--content">
				<?php echo $this->item->misc ?>
            </div>
		<?php endif; ?>

		<?php if ($this->params->get('show_info', 1)) : echo $this->loadTemplate('address'); endif; ?>

        <?php if ($this->params->get('show_email_form', 1)) : ?>
            <div class="page_contact--form-container">
                <div class="page_contact--map">
                    <?php echo JModuleHelper::renderModule($moduleMap[0], $attribs); ?>
                    <p class="page_contact--map-address">
                        <span><?php echo $this->contact->address ?>,</span><br>
                        <span><?php echo $this->contact->suburb ?>,</span><br>
                        <span><?php echo $this->contact->country ?>,</span>
                        <span><?php echo $this->contact->postcode ?></span>
                    </p>
                </div>
                <div class="page_contact--form">
                    <?php echo $this->loadTemplate('form'); ?>
                </div>
            </div>
        <?php endif; ?>
    </article>
</main>