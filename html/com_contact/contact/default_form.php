<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');

?>
<form action="<?php echo JRoute::_('index.php'); ?>" method="post" class="contact_form">
    <h3 class="contact_form--header"><?php echo JText::_('TPL_PROMASPECT_PREPARATION_OF_APPLICATION'); ?></h3>
    <p class="contact_form--description"><?php echo JText::_('TPL_PROMASPECT_FORM_DESCRIPTION'); ?></p>
	<?php foreach ($this->form->getFieldsets() as $fieldset)
	{
		$fields = $this->form->getFieldset($fieldset->name);
		if (count($fields))
		{
			echo '<fieldset class="contact_form--input-container">';
			foreach ($fields as $field)
			{
				if ($field->type === 'Spacer') continue;

				echo '<div class="contact_form--input-container">';
				switch ($field->type):
					case 'Text':
						echo " <input type='text' name='$field->name' id='$field->id' required aria-required='$field->required' class='contact_form--input' placeholder='" . JText::_($field->description) . "'>";
						echo "<p hidden data-hostid = '$field->id' class='contact_form--error'>" . JText::_('TPL_PROMASPECT_FORM_ERROR_FIELD') . "</p>";
						break;
					case 'Textarea':
						echo "<textarea name='$field->name' id='$field->id' required aria-required='$field->required' class='contact_form--input contact_form--input-textarea' placeholder='" . JText::_($field->description) . "'></textarea>";
						echo "<p hidden data-hostid = '$field->id' class='contact_form--error'>" . JText::_('TPL_PROMASPECT_FORM_ERROR_FIELD') . "</p>";
						break;
                    case 'Email':
                        echo " <input type='email' name='$field->name' id='$field->id' required aria-required='$field->required' class='contact_form--input' placeholder='" . JText::_($field->description) . "'>";
	                    echo "<p hidden data-hostid = '$field->id' class='contact_form--error'>" . JText::_('TPL_PROMASPECT_FORM_ERROR_FIELD') . "</p>";
                        break;
				endswitch;
				echo '</div>';
			}
			echo '</fieldset>';
		}

	} ?>

    <div class="control-group">
        <div class="controls">
            <div class="contact_form--private-container">
                <input type="checkbox" name="jform[politic]" id="jform_private_politic" class="contact_form--private-control">
                <label for="jform_private_politic" class="contact_form--private-control icon-btn-chech"></label>
                <p class="contact_form--private-description">
                    Я прочитал <a href="index.php?Itemid=117" target="_blank" class="contact_form-private-link">Политику конфиденциальности</a> и даю согласие на обработку персональных данных</p>
            </div>
            <button disabled class="page_article--button page_article--button-full" id="jform_btn_submit"
                    type="submit"><?php echo JText::_('COM_CONTACT_CONTACT_SEND'); ?></button>
            <input type="hidden" name="option" value="com_contact"/>
            <input type="hidden" name="task" value="contact.submit"/>
            <input type="hidden" name="return" value="<?php echo $this->return_page; ?>"/>
            <input type="hidden" name="id" value="<?php echo $this->contact->slug; ?>"/>
			<?php echo JHtml::_('form.token'); ?>
        </div>
    </div>

    <script>
        const button = jQuery('#jform_btn_submit');
        const plisseChecker = jQuery('#jform_private_politic');
        const formBlock = jQuery('div.contact_form');

        const form = jQuery('form');
        const fieldName = form.find('input[name="jform[contact_name]"]');
        const fieldMail = form.find('input[name="jform[contact_email]"]');
        const fieldSubject = form.find('input[name="jform[contact_subject]"]');
        const fieldMessage = form.find('textarea[name="jform[contact_message]"]');

        jQuery(plisseChecker).change(() => plisseChecker.attr('checked') ? button.removeAttr('disabled') : button.attr('disabled', 'disabled'));

    </script>
</form>