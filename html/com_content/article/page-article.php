<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

// Create shortcuts to some parameters.
$params  = $this->item->params;
$urls    = json_decode($this->item->urls);
$canEdit = $params->get('access-edit');
$user    = JFactory::getUser();
$info    = $params->get('info_block_position', 0);

$parameters = [];
foreach ($this->item->jcfields as $jcfield)
{
	$parameters[$jcfield->name] = $jcfield;
}

jimport('joomla.application.module.helper');
$module           = JModuleHelper::getModules('position-4');
$attribs['style'] = 'none';

$treeArray = JFactory::getApplication()->getMenu()->getActive()->tree;

// Check if associations are implemented. If they are, define the parameter.
$assocParam = (JLanguageAssociations::isEnabled() && $params->get('show_associations'));
JHtml::_('behavior.caption');

?>
<div class="header_title-container">
    <div class="header_title--image-container">
        <img src="/<?php echo json_decode($this->item->images)->image_fulltext ?>" alt=""
             class="header_title--image">
    </div>
</div>
<div class="header_title--name-wrapper">
    <h1><?php echo $this->item->title ?></h1>
</div>
</header>

<main class="container-fluid">
    <div class="container">
        <div>
		<?php echo $this->item->text ?>
        </div>
    </div>
</main>
