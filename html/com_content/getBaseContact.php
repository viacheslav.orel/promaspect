<?php

use Joomla\Registry\Registry;

JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_contact/models', 'ContactModel');
$model = JModelLegacy::getInstance('Contact', 'ContactModel', array('ignore_request' => true));
$model->setState('contact.id', 4);
$model->setState('params', new Registry);
$contact = $model->getItem();

$parameters = FieldsHelper::getFields('com_contact.contact', $contact);

function createSocialItem($parameter)
{
	return '<div class="social--container">
				<a href="' . $parameter->rawvalue . '" class="social--item">' . $parameter->title . '</a>
			</div>';
}

$socialLinkComponent = '<div class="social-wrapper">';
foreach ($parameters as $parameter)
{
	if ($parameter->title === 'Social media')
	{
		$socialLinkComponent .= createSocialItem($parameter);
	}
}
$socialLinkComponent .= '</div>';