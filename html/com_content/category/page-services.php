<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\Registry\Registry;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
jimport('joomla.application.module.helper');
require_once 'page-category_article.php';
require_once 'page-category_category.php';
require_once JPATH_THEMES . '\promaspect\html\com_content\BaseContact.php';
if (!class_exists('ContentModelArticle')) require_once JPATH_ROOT . '/components/com_content/models/article.php';


$document   = JFactory::getDocument();
$renderer   = $document->loadRenderer('modules');
$parameters = FieldsHelper::getFields('com_content.article');


include 'page-category_title-block.php';

function GetSubMenu($allMenu, $currentId)
{
	return array_filter($allMenu, function ($item) use ($currentId) {
		return $item->parent_id === $currentId;
	});
}

?>

<main class="container-fluid">
    <div class="container">
		<?php
		$allMenuItems = JMenu::getInstance('site')->getItems($attributes = array(), $values = array());

		$subMenus = GetSubMenu($allMenuItems, $this->menu->id); ?>

		<?php if ($subMenus):

			foreach ($subMenus

			         as $subMenu):

				if ($subMenu->type === 'heading'): ?>

                    <div class="card-wrapper">
                        <h2 class="card--header"><?php echo $subMenu->title; ?></h2>
                        <div class="cards cards--twoColumns">

							<?php foreach (GetSubMenu($allMenuItems, $subMenu->id) as $articleMenu):
								$article = new ContentModelArticle;
								$article = $article->getItem($articleMenu->query['id']); ?>

                                <div class="card--imageTop">
                                    <img src="<?php echo json_decode($article->images)->image_intro ?>" alt=""
                                         class="card--preview">
                                    <div class="card--content">
                                        <h3 class="card--title"><?php echo $articleMenu->title; ?></h3>
                                        <p class="card--content-text">
											<?php echo array_filter(FieldsHelper::getFields('com_content.article', $article),
												function ($item) {
													return $item->name === 'short-description';
												})[0]->value; ?>
                                        </p>
                                        <a href="<?php echo $articleMenu->route; ?>" class="card--link">
											<?php echo JText::_('TPL_PROMASPECT_KNOW_MORE') ?>
                                        </a>
                                    </div>
                                </div>

							<?php endforeach; ?>
                        </div>
                    </div>

				<?php endif;

			endforeach;

		endif; ?>
    </div>
</main>