<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\Registry\Registry;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
require_once JPATH_THEMES . '\promaspect\html\com_content\objects\Articles.php';
require_once JPATH_ROOT . '\components\com_contact\models\contact.php';

function GetSubMenu($allMenu, $currentId)
{
	return array_filter($allMenu, function ($item) use ($currentId) {
		return $item->parent_id === $currentId;
	});
}

function GetPreviewLink($contact)
{
	$parameters = FieldsHelper::getFields('com_contact.contact', $contact);

	foreach ($parameters as $parameter)
	{
		if ($parameter->name === 'image-preview')
			return $parameter->rawvalue;
	}
}

$allMenuItems = JMenu::getInstance('site')->getItems($attributes = array(), $values = array());
$subMenus     = GetSubMenu($allMenuItems, $this->menu->id);

include 'page-category_title-block.php'; ?>

<main class="container-fluid">
    <div class="container">
        <div class="page_article--content">
			<?php echo $this->category->description ?>

			<?php if ($subMenus): ?>
				<?php foreach ($subMenus as $subMenu): ?>
					<?php if ($subMenu->type === 'heading'): ?>
                        <div class="card-wrapper">
                            <div class="cards cards--threeColumns">
                                <h2 class="card--header"><?php echo $subMenu->title; ?></h2>
								<?php foreach (GetSubMenu($allMenuItems, $subMenu->id) as $contactMenu): ?>
									<?php $contact = new ContactModelContact;
									$contact       = $contact->getItem($contactMenu->query['id']); ?>
                                    <div class="cardContact--item">
                                        <img src="<?php echo GetPreviewLink($contact); ?>" alt=""
                                             class="cardContact--preview">
                                        <h3 class="cardContact--title"><?php echo $contact->name; ?></h3>
                                        <p class="cardContact--description"><?php echo $contact->con_position; ?></p>
                                        <a href="<?php echo $contactMenu->route; ?>" class="card--link">
											<?php echo JText::_('TPL_PROMASPECT_KNOW_MORE') ?>
                                        </a>
                                    </div>
								<?php endforeach; ?>
                            </div>
                        </div>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endif; ?>
        </div>
    </div>
</main>