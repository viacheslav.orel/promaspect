<?php

use Joomla\CMS\Router\Route;

function getCategoryBlock($category)
{
	$result = '';
	$categoryParameters = [];
	foreach (FieldsHelper::getFields('com_content.categories', $category) as $field)
	{
		$categoryParameters[$field->name] = $field;
	}

	switch ($categoryParameters['category-content-type']->rawvalue[0]):
		case '0':
			$result .= "<section class='content_categories--container'>
								<h2 class='content_categories--header'>$category->title</h2>
								<div class='content_categories--content-2column'>";

			foreach ($category->getChildren() as $child)
			{
				$result .= getCategoryBlock($child);
			}
			$result .= '</div></section>';
			break;
		case '2':
			$result .= '
							<div class="card">
								<article class="card--vertical">
									<img src="' . $categoryParameters['short-summary-image']->value . '" alt="" class="card--image">
									<div class="card--content">
										<h3 class="card--header">' . $category->title . '</h3>
										<p class="card--description">
											' . $categoryParameters['short-summary-text']->value . '
										</p>
									</div>
									<a href="' . Route::_(ContentHelperRoute::getCategoryRoute($category->id, $category->language)) . '" class="card--link">
										' . jtext::_('TPL_PROMASPECT_KNOW_MORE') . ' <span class="icon-arrow-toright"></span>
									</a>
								</article>
							</div>
						';
			break;
	endswitch;

	return $result;
}