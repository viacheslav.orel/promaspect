<?php

function contentBlock($articleItem)
{
	$image = json_decode($articleItem->images);

	foreach ($articleItem->jcfields as $field)
	{
		if ($field->name === 'content-type')
		{
			switch ($field->rawvalue[0]):
				case '1':
					return '
					<div class="card">
						<article class="card--horizontal card--horizontal-' . $image->float_fulltext . '">
							<h2 class="card--name">' . $articleItem->title . '</h2>
							<img src="' . $image->image_fulltext . '" alt="" class="card--image">
							<div class="card--content">
								<div class="card--description">' . $articleItem->text . '</div>
							</div>
						</article>
					</div>';
			endswitch;
		}
	}
}

function GetObjectCardBlock($menuItem, $articleItem)
{
	if(!class_exists('ContentModelArticle')) require_once JPATH_ROOT.'/components/com_content/models/article.php';
	$articles = new ContentModelArticle;
	$article = $articles->getItem($articleItem->id);
	$articleParameters = FieldsHelper::getFields('com_content.article', $article);
	$parameters = [];

	foreach ($articleParameters as $parameter)
	{
		$parameters[$parameter->name] = $parameter;
	}

	$cityId = json_decode($parameters['object-city']->rawvalue)->id;

	return "<div class='card' data-city='$cityId'>
				<article class='card--vertical card--vertical-object'>
					<div class='card--image-container'>
						<img src='/{$parameters['short-image']->rawvalue}' alt='' class='card--image'>
						<a href='$menuItem->route' class='card--image_full-container'>
						<img src='/{$parameters['short-summary-image-full']->rawvalue}' alt='' class='card--image_full'>
						</a>
					</div>
					<div class='card--content'>
						<h3 class='card--header'>$menuItem->title</h3>
					</div>
					<a href='$menuItem->route' class='card--link'>" . JText::_('TPL_PROMASPECT_KNOW_MORE') . " <span class='icon-arrow-toright'></span></a>
				</article>
			</div>";
}