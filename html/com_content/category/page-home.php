<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
require_once 'page-category_article.php';

$parameters = FieldsHelper::getFields('com_content.categories', $this->category); ?>

<div class="header_title-fullContainer">
    <div class="header_title--image-container ">
        <img src="<?php echo $this->category->params['image'] ?>" alt="" class="header_title--image">
    </div>
    <div class="container-fluid company_info">
        <div class="container">
            <h1 class="company_info--name"><?php echo JFactory::getConfig()->get('sitename') ?></h1>
            <p class="company_info--description"><?php echo JText::_('TPL_PROMASPECT_COMPANY_DESCRIPTION') ?></p>
        </div>
    </div>
</div>
</header>

<main class="container-fluid">
    <div class="container">
		<?php echo $this->category->description ?>
    </div>

	<?php
	require_once JPATH_THEMES . '\promaspect\html\com_content\objects\Articles.php';
	$subMenus = JMenu::getInstance('site')->getItems($attributes = array(), $values = array());

	if ($subMenus)
	{
		$articles = new Articles($subMenus, $this->menu->id);
		echo $articles->RenderArticles();
	}

	JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_contact/models', 'ContentModel');
	$categoryModel = JModelLegacy::getInstance('Category', 'ContactModel', array('ignore_request' => true));
	$categoryModel->setState('category.id', 16);
	$contacts = $categoryModel->getItems();
	?>

    <div class="container">
        <div class="ourPartners-wrapper">
            <h2 class="card--header"><?php echo JText::_('TPL_PROMASPECT_OUR_PARTNERS') ?></h2>
            <div class="ourPartners-container">
                <div class="ourPartners--row">
					<?php
					$prevQuantity = 3;
					$breakRow     = '</div><div class="ourPartners--row">'; ?>

					<?php for ($i = 0; $i < count($contacts); $i++): ?>
                        <a href="<?php echo $contacts[$i]->webpage ?>" target="_blank" class="ourPartner--link">
                            <img src="<?php echo $contacts[$i]->image ?>" alt="" class="ourPartner--image">
                        </a>

						<?php if ($i !== 0 && $prevQuantity === 3 && ($i + 1) % 4 === 0)
						{
							echo $breakRow;
							$prevQuantity = 4;
						}
                        elseif ($i !== 0 && $prevQuantity === 4 && ($i + 1) % 4 === 0)
						{
							echo $breakRow;
							$prevQuantity = 3;
						} ?>
					<?php endfor; ?>
                </div>
            </div>
        </div>
    </div>

</main>
