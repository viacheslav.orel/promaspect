<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

use Joomla\Registry\Registry;

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
require_once 'page-category_article.php';
if (!class_exists('ContentModelArticle')) require_once JPATH_ROOT . '/components/com_content/models/article.php';

$document = JFactory::getDocument();
$renderer = $document->loadRenderer('modules');


$parameters = FieldsHelper::getFields('com_content.article');

$objectCity = "";
$cityName   = 'name-' . JFactory::getLanguage()->getLocale()[6];

foreach ($parameters as $parameter)
{
	switch ($parameter->name):
		case 'object-city':
			$objectCity = $parameter;
			break;
	endswitch;
}
?>

<div class="header_title-container bg-secondary-dark container-fluid">
    <div class="container">
        <div class="object_map-wrapper">
            <div class="object_map-container">
                <img class="object_map" src="images\object\header-bg.svg" alt="">
				<?php foreach ($objectCity->fieldparams['options'] as $key => $fieldparam)
				{
					$objectInfo      = json_decode($fieldparam->value);
					$objectVariables = get_object_vars($objectInfo);
					echo '<div class="object_map--city" data-target="' . $objectInfo->id . '" style="top: ' . $objectInfo->top . '; left: ' . $objectInfo->left . '">
                        <p class="object_map--city-description" style="top: ' . $objectInfo->nameTop . '; left:' . $objectInfo->nameLeft . '">' . $objectVariables[$cityName] . '</p>
                    </div>';
				} ?>
            </div>
        </div>
    </div>

    <div class="header_title--name-wrapper">
        <h1><?php echo $this->category->title ?></h1>
    </div>
</div>
</header>

<main class="container-fluid">
    <div class="container">
        <h2><?php echo JText::_('TPL_PROMASPECT_OBJECT_DESCRIPTION') ?></h2>
        <div class="page_object--container">
			<?php require_once JPATH_THEMES . '\promaspect\html\com_content\objects\Articles.php';
			$subMenus = array_filter(JMenu::getInstance('site')->getItems($attributes = array(), $values = array()),
				function ($item) {
					return $item->parent_id === $this->menu->id;
				}); ?>
			<?php if ($subMenus): ?>
				<?php foreach ($subMenus as $subMenu):
					$article = new ContentModelArticle;
					$article = $article->getItem($subMenu->query['id']);

					$fields = FieldsHelper::getFields('com_content.article', $article);
					$cityId = '';
					foreach ($fields as $field)
					{
                        if ($field->name === 'object-city')
                        {
	                        $cityId = json_decode($field->rawvalue)->id;
                        }
					}
					?>
                    <div class="page_object--item" data-city="<?php echo $cityId ?>">
                        <div class="page_object--preview-wrapper">
                            <a href="<?php echo $subMenu->route ?>" class="page_object--preview-container">
                                <img src="<?php echo json_decode($article->images)->image_intro ?>" alt=""
                                     class="page_object--preview">
                            </a>
                        </div>
                        <h3 class="page_object--title"><?php echo $subMenu->title ?></h3>
                        <a href="<?php echo $subMenu->route ?>"
                           class="card--link"><?php echo JText::_('TPL_PROMASPECT_KNOW_MORE') ?></a>
                    </div>
				<?php endforeach; ?>
			<?php endif; ?>
        </div>
    </div>
</main>

<script>
    jQuery(document).ready($ => {

        const cities = $('.object_map--city');
        const objects = $('.page_object--item');
        let hoverCity;

        const activeCity = (cities, object, hoverCity) => {
            const hoverCityId = hoverCity.dataset.target;

            cities.each((index, item) => {
                if (item !== hoverCity) {
                    $(item).addClass('object_map--city-gray');
                } else
                    $(item).removeClass('object_map--city-gray');
            });

            object.each((index, item) => {
                if (item.dataset.city !== hoverCityId)
                    $(item).attr('hidden', 'hidden');
                else
                    $(item).removeAttr('hidden');
            });

            $(hoverCity).addClass('object_map--city-hover');
        }

        cities.click(event => {
            if (hoverCity) {
                $(hoverCity).removeClass('object_map--city-hover');

                if (hoverCity !== event.currentTarget) {
                    hoverCity = event.currentTarget;
                    activeCity(cities, objects, hoverCity);
                } else {
                    hoverCity = null;
                    cities.each((index, item) => $(item).removeClass('object_map--city-gray'));
                    objects.each((index, item) => $(item).removeAttr('hidden'));
                }
            } else {
                hoverCity = event.currentTarget;
                activeCity(cities, objects, hoverCity);
            }
        });
    });
</script>
