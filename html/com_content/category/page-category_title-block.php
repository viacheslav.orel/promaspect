<?php ?>
<div class="header_title-container">
	<div class="header_title--image-container">
		<img src="<?php echo $this->category->params['image'] ?>" alt="" class="header_title--image">
	</div>
	<div class="header_title--name-wrapper">
		<h1><?php echo $this->category->title ?></h1>
	</div>
</div>
</header>