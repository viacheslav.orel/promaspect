<?php

use Joomla\Registry\Registry;

class BaseContact
{
	public $baseContact, $baseContactFields, $orderedField, $socialLinkComponent, $menuItem, $shortImage;

	function __construct($contactId, $menuItem = null)
	{
		$this->menuItem = $menuItem;

		JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_contact/models', 'ContactModel');
		$model = JModelLegacy::getInstance('Contact', 'ContactModel', array('ignore_request' => true));
		$model->setState('contact.id', $contactId);
		$model->setState('params', new Registry);
		$this->baseContact = $model->getItem();

		$this->baseContactFields = FieldsHelper::getFields('com_contact.contact', $this->baseContact);
		foreach ($this->baseContactFields as $baseContactField)
		{
			$this->orderedField[$baseContactField->name] = $baseContactField;
		}
	}

	private function CreateSocialItem($parameter)
	{
		return '<div class="social_media--item-container">
				<a target="_blank" href="' . $parameter->rawvalue . '" class="social_media--item">' . $parameter->title . '</a>
			</div>';
	}

	public function GetSocialLinkComponent()
	{
		$this->socialLinkComponent = '<div class="social_media">';

		foreach ($this->baseContactFields as $parameter)
		{
			if ($parameter->group_title === 'Social media')
			{
				$this->socialLinkComponent .= $this->createSocialItem($parameter);
			}
		}

		$this->socialLinkComponent .= '</div>';

		return $this->socialLinkComponent;
	}

	public function GetShortImage()
	{
		foreach ($this->baseContactFields as $parameter)
		{
			if ($parameter->name === 'contact-short-image')
			{
				return $parameter->value;
			}
		}
	}
}