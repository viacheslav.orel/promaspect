<?php


class Articles
{
	public $articles = [];

	function __construct($menuItems, $currentMenuId)
	{
		if (!class_exists('ContentModelArticle')) require_once JPATH_ROOT . '/components/com_content/models/article.php';

		foreach ($menuItems as $item)
		{
			if ($item->parent_id === $currentMenuId)
			{
				$articles = new ContentModelArticle;
				$article  = $articles->getItem($item->query['id']);

				array_push($this->articles, array($item, $article));
			}
		}
	}

	private function GetFields($article)
	{
		$fields     = FieldsHelper::getFields('com_content.article', $article);
		$parameters = [];


		foreach ($fields as $field)
		{
			$parameters[$field->name] = $field;
		}

		return $parameters;
	}

	private function GetCardClass($image)
	{
		switch ($image->float_intro):
			case 'left':
				return 'card card--imageLeft';
			case 'right':
				return 'card card--imageRight';
			default:
				return 'card';
		endswitch;
	}

	private function GetHeader($visible, $menu, $contentClass = '')
	{
		if (in_array('header', $visible))
		{
			$style = 'card--header' . ($contentClass != '' ? ' cards--header-' . $contentClass : '');

			return "<h2 class='$style'>$menu->title</h2>";
		}

		return '';
	}

	private function GetBlockName($visible, $article, $contentClass = '')
	{
		if (in_array('name', $visible))
		{
			$style = 'card--title';

			if ($contentClass != '')
			{
				$style .= ' ' . $style . '-' . $contentClass;
			}

			return "<h3 class='$style'>{$article->title}</h3>";
		}

		return '';
	}

	private function GetBlockDescription($visible, $parameters, $contentClass = '')
	{
		if (in_array('description', $visible))
		{
			$result = "<div class='card--content-text'>";

			foreach (explode(PHP_EOL, $parameters['short-description']->value) as $paragraph)
			{
				$result .= "<p>$paragraph</p>";
			}

			$result .= "</div>";

			return $result;
		}

		return '';
	}

	private function GetBlockLink($visible, $menu, $contentClass = '')
	{
		if (in_array('link', $visible))
		{
			return "<a href='$menu->route' class='card--link'>" . jtext::_('TPL_PROMASPECT_KNOW_MORE') . "</a>";
		}

		return '';
	}

	private function GetImage($visible, $image)
	{
		return in_array('preview', $visible)
			? "
				<div class='card--preview'>
					<img src='/{$image->image_intro}' alt='' />
				</div>
			"
			: '';
	}

	private function GetContent($visible, $article, $parameters, $menu, $contentClass = '')
	{
		$result = "<div class='card--content'>";
		$result .= $this->GetBlockName($visible, $article, $contentClass);
		$result .= $this->GetBlockDescription($visible, $parameters, $contentClass);
		$result .= $this->GetBlockLink($visible, $menu, $contentClass);
		$result .= '</div>';

		return $result;
	}

	public function RenderArticles()
	{
		$result = '';

		foreach ($this->articles as $article)
		{
			$parameters = $this->GetFields($article[1]);
			$image      = json_decode($article[1]->images);

			switch ($parameters['block-type-orientation']->rawvalue):
				case 'horizontal':
					$result .= "<div class='container card-wrapper'><div class='{$this->GetCardClass($image)}'>";
					$result .= $this->GetHeader($parameters['visible']->rawvalue, $article[0]);

					$result .= $this->GetImage($parameters['visible']->rawvalue, $image);

					$result .= $this->GetContent($parameters['visible']->rawvalue, $article[1], $parameters, $article[0]);

					$result .= '</div></div>';
					break;
				case 'vertical':
					break;
				case 'individual':
					$result .= "<div class='card-wrapper'><div class='card card--highlighted'>";
					$result .= $this->GetImage($parameters['visible']->rawvalue, $image);

					$result .= "<div class='card--content'>";
					$result .= $this->GetHeader($parameters['visible']->rawvalue, $article[0]);
					$result .= $this->GetBlockDescription($parameters['visible']->rawvalue, $parameters);
					$result .= $this->GetBlockLink($parameters['visible']->rawvalue, $article[0]);
					$result .= "</div>";

					$result .= "</div></div>";
					break;
			endswitch;
		}

		return $result;
	}
}