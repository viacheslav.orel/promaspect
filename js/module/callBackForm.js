"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var CallBackForm = /*#__PURE__*/function () {
  function CallBackForm($body, $formContainer) {
    _classCallCheck(this, CallBackForm);

    this.$body = $body;
    this.$openBtn = $formContainer.find('#call-back--btn');
    this.$callBackForm = $formContainer.find('form');
    this.$callBackLayer = $formContainer.find('.call-back--container');
    this._isFormOpen = false;
  }

  _createClass(CallBackForm, [{
    key: "subscribeToEvent",
    value: function subscribeToEvent() {
      var _this = this;

      this.$openBtn.click(function () {
        return _this._isFormOpen ? _this._formClose() : _this._formOpen();
      });
    }
  }, {
    key: "_formOpen",
    value: function _formOpen() {
      this.$callBackLayer.addClass('call-back--container-active');
      this.$body.addClass('overflow-hide');
      this._isFormOpen = true;
    }
  }, {
    key: "_formClose",
    value: function _formClose() {
      this.$callBackLayer.removeClass('call-back--container-active');
      this.$body.removeClass('overflow-hide');
      this._isFormOpen = false;
    }
  }]);

  return CallBackForm;
}();

var _default = CallBackForm;
exports["default"] = _default;
//# sourceMappingURL=callBackForm.js.map