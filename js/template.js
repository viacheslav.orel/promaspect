"use strict";

jQuery.when(jQuery.ready).then(function ($) {
    const $openBtn = $('.callback--btn');
    const $callBackFormContainer = $('.callback--container');
    const $callBackForm = $('form.callback--form');
    const $callBackFormSuccess = $('.callback--form-success');

    $openBtn.on('click', function () {
        $callBackFormContainer.toggleClass('callback--container-active');
        $callBackForm.removeAttr('hidden');
        $callBackFormSuccess.attr('hidden', 'hidden')
    });
    $callBackFormContainer.on('click', function (event) {
        if ($(event.target).is($callBackFormContainer) ||
            $(event.target).is('.callback--close') ||
            $(event.target).is('button[type="button"]')) {
            $callBackFormContainer.toggleClass('callback--container-active');

            $callBackForm[0].reset();
        }
    });

    $callBackForm.on('submit', event => {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '\\modules\\mod_callback_form\\mod_callback_form.php',
            data: '',
            success: () => {
                $callBackForm.attr('hidden', 'hidden');
                $callBackFormSuccess.removeAttr('hidden');
            },
        })
    });
});