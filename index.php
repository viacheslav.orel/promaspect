<?php
/**
 * @package    Joomla.Site
 * @subpackage Template.promaspect
 *
 * @author     Viacheslav.Orel <your@email.com>
 * @copyright  A copyright
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       http://your.url.com
 */

defined('_JEXEC') or die;

use Joomla\CMS\Language\Text;

require_once JPATH_THEMES . '/' . $this->template . '/helper.php';
require_once JPATH_THEMES . '\promaspect\html\com_content\BaseContact.php';

JHtml::_('jquery.framework'); // загружаем jquery
tplPromaspectHelper::loadCss();
tplPromaspectHelper::loadJs();
tplPromaspectHelper::setMetadata();

$app      = JFactory::getApplication();
$menu     = $app->getMenu();
$lang     = JFactory::getLanguage();
$thisPage = $menu->getActive();
$isHeader = $thisPage === $menu->getDefault($lang->getTag());

$baseContact = new BaseContact(4);

$document  = JFactory::getDocument();
$config    = JFactory::getConfig();
$fulltitle = $config->get('sitename') . ' - ' . $document->title;
$document->setTitle($fulltitle);

?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600&display=swap" rel="stylesheet">
    <jdoc:include type="head"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body>
<?php echo tplPromaspectHelper::setAnalytics(0, 'your-analytics-id'); ?>

<header>
    <div class="callback--wrapper">
        <button class="callback--btn icon-btn-callback"></button>
        <div class="callback--container">
			<jdoc:include type="modules" name="position-5" style="..."/>
        </div>
    </div>
	<?php if (!$isHeader): ?>
        <div class="breadcrumb-wrapper container-fluid">
            <div class="container">
                <jdoc:include type="modules" name="position-2" style="..."/>
            </div>
        </div>
		<?php echo $baseContact->GetSocialLinkComponent(); endif; ?>

    <div class="container-fluid <?php echo $isHeader ? 'header--content-home_page' : '' ?>">
        <div class="container">
            <div class="header--content-container ">
                <a class="my-logo--wrapper" href="/">
                    <img src="/images/home/promaspect-logo.svg" alt="" class="my-logo">
                </a>
                <jdoc:include type="modules" name="position-0" style="..."/>
                <jdoc:include type="modules" name="position-1" style="..."/>
            </div>
        </div>
    </div>

    <jdoc:include type="component" style="..."/>

    <footer class="container-fluid">
        <div class="container page_footer-container">
            <div class="page_footer--contact-container">
                <div class="page_footer--contact">
                    <h3 class="page_footer--contact-header"><?php echo JText::_('TPL_PROMASPECT_CALL_US') ?></h3>
                    <div class="page_footer--contact-content">
						<?php foreach (explode(';', $baseContact->baseContact->telephone) as $phone) :
							$phoneNumber = str_replace(array('(', ')', '-', ' '), '', $phone); ?>
                            <a href="tel:<?php echo $phoneNumber ?>"
                               class="page_footer--contact-phone"><?php echo $phone ?></a>
						<?php endforeach; ?>
						<?php if ($baseContact->baseContact->mobile):
							foreach (explode(';', $baseContact->baseContact->mobile) as $phone) :
								$phoneNumber = str_replace(array('(', ')', '-', ' '), '', $phone); ?>
                                <a href="tel:<?php echo $phoneNumber ?>"
                                   class="page_footer--contact-phone"><?php echo $phone ?></a>
							<?php endforeach;
						endif; ?>
                    </div>
                </div>
                <div class="page_footer--contact">
                    <h3 class="page_footer--contact-header"><?php echo JText::_('TPL_PROMASPECT_WRITE_TO_US') ?></h3>
                    <div class="page_footer--contact-content">
						<?php foreach (explode(';', $baseContact->orderedField['mail']->value) as $mail): ?>
                            <a href="mailto:<?php echo $mail ?>" class="contact_us--link"><?php echo $mail ?></a>
						<?php endforeach; ?>
                    </div>
                </div>
                <div class="page_footer--contact">
                    <h3 class="page_footer--contact-header"><?php echo JText::_('TPL_PROMASPECT_VISIT_US') ?></h3>
                    <div class="page_footer--contact-content">
                        <p class="page_footer--contact-address">
							<?php if ($baseContact->baseContact->country): ?>
                                <span><?php echo $baseContact->baseContact->country ?>, </span>
							<?php endif; ?>
							<?php if ($baseContact->baseContact->postcode): ?>
                                <span><?php echo $baseContact->baseContact->postcode ?>, </span>
							<?php endif; ?>
							<?php if ($baseContact->baseContact->suburb): ?>
                                <span><?php echo $baseContact->baseContact->suburb ?>,</span>
							<?php endif; ?>
                            <br>
							<?php if ($baseContact->baseContact->address): ?>
                                <span><?php echo $baseContact->baseContact->address ?>, </span>
							<?php endif; ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="page_footer--axillary">
                <div class="page_footer--socialMedia">
					<?php foreach ($baseContact->baseContactFields as $field):
						if ($field->group_title === 'Social media'):?>
                            <a href="<?php echo $field->rawvalue; ?>" target="_blank"
                               class="page_footer--socialMedia-item icon-btn-<?php echo $field->name ?>"></a>
						<?php endif;
					endforeach; ?>
                </div>
                <div class="page_footer--technicalInfo">
                    <a href="<?php echo JRoute::_('index.php?Itemid=117') ?>"
                       class="page_footer--privatePolice"><?php echo JText::_('TPL_PROMASPECT_PRINATE_POLICE') ?></a>
                    <p class="page_footer--technicalText">&copy;PROMASPECT, 2020</p>
                    <p class="page_footer--technicalText">DESIGN & DEV: <a href="http:\\orslav-team.com" target="_blank"
                                                                           class="page_footer--technicalText">ORSLAVTEAM</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <jdoc:include type="modules" name="debug" style="none"/>
</body>
</html>
